#include <cstdio>
#include <string>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"

//setter for the king coordinate
void ChessGame::setKingCoords(Position pset, Player p) {
    if (p == WHITE) whiteKingCoords = pset;
    else blackKingCoords = pset;
}

//getter for the king position
Position ChessGame::getKingCoords(Player p) {
    if (p == WHITE) return whiteKingCoords;
    else return blackKingCoords;
}

//getter for the check state of the king
bool ChessGame::getCheckState(Player p) {
    if (p == WHITE) return whiteCheckState;
    else return blackCheckState;
}

//setter for the check state of the king to true
void ChessGame::trueCheckState(Player p) {
    if (p == WHITE) whiteCheckState = true;
    else blackCheckState = true;
}

//setter for the check state of king to false
void ChessGame::falseCheckState(Player p) {
    if (p == WHITE) whiteCheckState = false;
    else blackCheckState = false;
}

//this will check if the king has moved before castling.
void ChessGame::incrementCastleCount(Piece* p, int* cW, int* cB) {
    if (p->owner() == WHITE) {
        *cW = 1;
    } else {
        *cB = 1;
    }
}

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    Player pl = playerTurn();
    static int countWhite = 0;
    static int countBlack = 0;
    Piece* aPiece = getPiece(start);
    if (!validPosition(start) || !validPosition(end)) {
        Prompts::outOfBounds();
        return MOVE_ERROR_OUT_OF_BOUNDS;
    }
    if ((validPosition(start) && aPiece == nullptr) || aPiece->owner() != pl ) {
        Prompts::noPiece();
        return MOVE_ERROR_NO_PIECE;
    }
    int moveType = aPiece->validMove(start, end, *this);
    if (moveType == MOVE_ERROR_ILLEGAL) {
        Prompts::illegalMove();
        return MOVE_ERROR_ILLEGAL;
    }
    if (moveType == MOVE_ERROR_BLOCKED) {
        Prompts::blocked();
        return MOVE_ERROR_BLOCKED;
    }
    Piece* dp = m_pieces[index(end)]; //deletion of captured piece pending after checking for checks
    if (moveType == MOVE_CAPTURE) {
        //Prompts::capture(pl);    //don't return yet
        m_pieces[index(end)] = aPiece;
        m_pieces[index(start)] = nullptr;
        if (aPiece->id() == ROOK_ENUM || aPiece->id() == KING_ENUM) {
            incrementCastleCount(aPiece, &countWhite, &countBlack);
        }
    }
    //pawn promotion
    if ((aPiece->id() == PAWN_ENUM) && ((aPiece->owner() == BLACK && start.y == 1)
                                        || (aPiece->owner() == WHITE && start.y == 6))) {
        makeQueen(start, end, pl);
    }

    Player enem = (Player)((pl + 1) % 2);
    Position friendlyKing = this->getKingCoords(pl);
    Position enemyKing = this->getKingCoords(enem);
    if (moveType == TRY_CASTLE) {
        int diffx = end.x - start.x;
        if (aPiece->owner() == WHITE ) {
            if (countWhite == 0 ) {
                if (diffx > 0 && !isChecked(friendlyKing, pl) && !isChecked({friendlyKing.x + 1, 0}, pl)
                        && !isChecked({friendlyKing.x + 2, 0}, pl)) {
                    m_pieces[index(end)] = aPiece;
                    m_pieces[index(start)] = nullptr;
                    Piece* rookCastle = this->getPiece({ 7, 0 });
                    m_pieces[index({ 5, 0 })] = rookCastle;
                    m_pieces[index({ 7, 0 })] = nullptr;
                }
                else if (diffx < 0 && !isChecked(friendlyKing, pl) && !isChecked({friendlyKing.x - 1, 0}, pl)
                         && !isChecked({friendlyKing.x - 2, 0}, pl) && !isChecked({friendlyKing.x - 3, 0}, pl)) {
                    m_pieces[index(end)] = aPiece;
                    m_pieces[index(start)] = nullptr;
                    Piece* rookCastle = this->getPiece({ 0, 0 });
                    m_pieces[index({ 3, 0 })] = rookCastle;
                    m_pieces[index({ 0, 0 })] = nullptr;
                }
                else { //in case countWhite = 0 but king passes through check
                    Prompts::cantCastle();
                    return MOVE_ERROR_CANT_CASTLE;
                }
            } else {
                Prompts::cantCastle();
                return MOVE_ERROR_CANT_CASTLE;
            }
        } else {
            if (countBlack == 0) {
                if (diffx > 0 && !isChecked(friendlyKing, pl) && !isChecked({friendlyKing.x + 1, 7}, pl)
                        && !isChecked({friendlyKing.x + 2, 7}, pl)) {
                    m_pieces[index(end)] = aPiece;
                    m_pieces[index(start)] = nullptr;
                    Piece* rookCastle = this->getPiece({ 7, 7 });
                    m_pieces[index({ 5, 7 })] = rookCastle;
                    m_pieces[index({ 7, 7 })] = nullptr;
                }
                else if (diffx < 0 && !isChecked(friendlyKing, pl) && !isChecked({friendlyKing.x - 1, 7}, pl)
                         && !isChecked({friendlyKing.x - 2, 7}, pl) && !isChecked({friendlyKing.x - 3, 7}, pl)) {
                    m_pieces[index(end)] = aPiece;
                    m_pieces[index(start)] = nullptr;
                    Piece* rookCastle = this->getPiece({ 0, 7 });
                    m_pieces[index({ 3, 7 })] = rookCastle;
                    m_pieces[index({ 0, 7 })] = nullptr;
                }
                else {
                    Prompts::cantCastle();
                    return MOVE_ERROR_CANT_CASTLE;
                }
            } else {
                Prompts::cantCastle();
                return MOVE_ERROR_CANT_CASTLE;
            }
        }
    }


    //regular noncapture move
    if (moveType == SUCCESS) {
        m_pieces[index(end)] = aPiece;
        m_pieces[index(start)] = nullptr;
        if (aPiece->id() == ROOK_ENUM || aPiece->id() == KING_ENUM) {
            incrementCastleCount(aPiece, &countWhite, &countBlack);
        }
    }
    if (aPiece->id() ==
            KING_ENUM) { //player's new king coordinate AFTER SUCCESS of reg move, capture, or castle: critical for check/checkmate
        this->setKingCoords(end, pl);
    }

    friendlyKing = this->getKingCoords(pl);//re-get coords
    enemyKing = this->getKingCoords(enem);
    bool friendlyChecked = this->isChecked(friendlyKing, pl);
    bool enemyChecked = this->isChecked(enemyKing, enem);
    bool prevCheckState = getCheckState(pl);
    if (!enemyChecked) {
        falseCheckState(enem);
    }
    if (!friendlyChecked) {
        falseCheckState(pl);
    }

    //already in check from enemy
    if (friendlyChecked && prevCheckState == true) {
        Prompts::mustHandleCheck();
        if (dp != nullptr) {
            m_pieces[index(start)] = aPiece;
            m_pieces[index(end)] = dp;
        } else {
            m_pieces[index(start)] = aPiece;
            m_pieces[index(end)] = nullptr;
        }
        if (aPiece->id() == KING_ENUM) { //reverse stored king position
            this->setKingCoords(start, pl);
        }
        return MOVE_ERROR_MUST_HANDLE_CHECK;
    }
    //player exposed himself on this turn
    if (friendlyChecked && !prevCheckState) {
        Prompts::cantExposeCheck();
        //reverse piece move, restore captured enemy piece if necessary
        if (dp != nullptr) {
            m_pieces[index(start)] = aPiece;
            m_pieces[index(end)] = dp;
        } else {
            m_pieces[index(start)] = aPiece;
            m_pieces[index(end)] = nullptr;
        }
        if (aPiece->id() == KING_ENUM) {
            this->setKingCoords(start, pl);
        }
        return MOVE_ERROR_CANT_EXPOSE_CHECK;
    }

    if (enemyChecked) {
        trueCheckState(enem);
        bool checkmate = checkMate(enemyKing, enem);
        if (checkmate) {
            Prompts::checkMate(pl);
            Prompts::win(pl, m_turn);
            if (dp != nullptr) {
                delete dp;   //in case capture a piece while stalemate
            }
            return MOVE_CHECKMATE;
        }
        if (dp != nullptr) {
            delete dp;
        }
        Prompts::check(pl);
        return MOVE_CHECK;
    }
    bool ch = getCheckState(enem);
    bool stale = Stalemate(enem, this->getKingCoords(enem));
    if (!ch) {
        if (stale) {
            Prompts::staleMate();
            if (dp != nullptr) {
                delete dp;
            }
            return MOVE_STALEMATE;
        }
    }
    if (moveType == MOVE_CAPTURE) {
        if (dp != nullptr) {
            delete dp;
        }
        Prompts::capture(pl);
        return MOVE_CAPTURE;
    }
    return SUCCESS;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

// this function will help display the board.
void ChessGame::displayBoard() {
    Terminal t;
    t.colorBg(Terminal::BLACK);
    bool alter = true;
    for (unsigned int y = 0; y < 8; y++) {
        t.colorFg(true, Terminal::RED);
        t.colorBg(Terminal::BLACK);
        std::cout << (8 - y) << ' ';
        for (unsigned int x = 0; x < 8; x++) {
            alter = !alter;
            if (alter) t.colorBg(Terminal::CYAN);
            else t.colorBg(Terminal::YELLOW);
            Position temp = { x, 7 - y };
            if (this->getPiece(temp) == nullptr) {
                std::cout << "  ";
            }  else {
                Piece* tempPiece = this->getPiece(temp);
                if (tempPiece->owner() == WHITE) {
                    t.colorFg(true, Terminal::WHITE);
                    if (tempPiece->id() == PAWN_ENUM)
                        std::cout << "\u2659 ";
                    else if (tempPiece->id() == ROOK_ENUM)
                        std::cout << "\u2656 ";
                    else if (tempPiece->id() == KNIGHT_ENUM)
                        std::cout << "\u2658 ";
                    else if (tempPiece->id() == BISHOP_ENUM)
                        std::cout << "\u2657 ";
                    else if (tempPiece->id() == QUEEN_ENUM)
                        std::cout << "\u2655 ";
                    else if (tempPiece->id() == KING_ENUM) {
                        if (this->getCheckState(tempPiece->owner()) == true)
                            t.colorFg(false, Terminal::RED);
                        std::cout << "\u2654 ";
                        t.colorFg(true, Terminal::WHITE);
                    } else if (tempPiece->id() == MYSTERY_ENUM)
                        std::cout << "\u2616 ";
                } else {
                    t.colorFg(true, Terminal::BLACK);
                    if (tempPiece->id() == PAWN_ENUM)
                        std::cout << "\u265F ";
                    else if (tempPiece->id() == ROOK_ENUM)
                        std::cout << "\u265C ";
                    else if (tempPiece->id() == KNIGHT_ENUM)
                        std::cout << "\u265E ";
                    else if (tempPiece->id() == BISHOP_ENUM)
                        std::cout << "\u265D ";
                    else if (tempPiece->id() == QUEEN_ENUM)
                        std::cout << "\u265B ";
                    else if (tempPiece->id() == KING_ENUM) {
                        if (this->getCheckState(tempPiece->owner()) == true)
                            t.colorFg(false, Terminal::RED);
                        std::cout << "\u265A ";
                        t.colorFg(true, Terminal::BLACK);
                    } else if (tempPiece->id() == MYSTERY_ENUM)
                        std::cout << "\u2617 ";
                }
            }
        }
        std::cout << '\n';
        alter = !alter;
    }
    t.colorFg(true, Terminal::RED);
    t.colorBg(Terminal::BLACK);
    std::cout << "  A B C D E F G H " << std::endl;
    t.colorFg(false, Terminal::DEFAULT_COLOR);
}

//save mode for the user input
void ChessGame::saveMode() {
    Prompts::saveGame();
    std::string fileName;
    std::cin >> fileName;
    std::ofstream ofs;
    ofs.open(fileName);
    if (!ofs.is_open()) {
        Prompts::saveFailure();
        return;
    }
    ofs << "chess" << std::endl;
    ofs << (this->turn()) - 1 << std::endl;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Piece* pc = m_pieces[index(Position(i, j))];
            if (pc != nullptr) {
                char x[8] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
                ofs << pc->owner() << ' ' << x[i] << j + 1 << ' ' << pc->id() << std::endl;
            }
        }
    }
    ofs.close();
}

// the main program to run
int ChessGame::run() {
    Player pl = playerTurn();
    Prompts::playerPrompt(pl, m_turn);
    std::string pos, nextPos;
    std::cin >> pos;
    if (pos == "q" || pos == "Q") return 0;
    else if (pos == "save") {
        saveMode();
        return 0;
    } else if (pos == "forfeit") {
        Prompts::win(Player((pl + 1) % 2), m_turn);
        Prompts::gameOver();
        return 0;
    } else if (pos == "board") {
        return 2;
    } else if (pos.length() != 2) {
        Prompts::parseError();
        std::cin.clear();
    } else {
        std::cin >> nextPos;
        if (nextPos.length() != 2) {
            Prompts::parseError();
            std::cin.clear();
            run();
        } else {
            Position pnextPos = Position(tolower(nextPos[0]) - 97, tolower(nextPos[1]) - 49);
            Position ppos = Position(tolower(pos[0]) - 97, tolower(pos[1]) - 49);
            int moveMade = makeMove(ppos, pnextPos);
            if (moveMade < 1) {
                std::cin.clear();
                return 1;
            } else if (moveMade == MOVE_STALEMATE || moveMade == MOVE_CHECKMATE) {
                Prompts::gameOver();
                return 0;
            } else {
                m_turn++;
                return 1;
            }
        }
    }
    return 1;
}

//check valid move from pawn
int Pawn::validMove(Position start, Position end, const Board& board) const {
    Piece* pend = board.getPiece(end);
    if (board.playerTurn() == WHITE) {
        if (start.y == 1 && end.y == 3 &&
                (board.getPiece({start.x, start.y + 2 }) != nullptr
                 || board.getPiece({start.x, start.y + 1}) != nullptr)) return
                         MOVE_ERROR_BLOCKED;    //blocked on double jump
        else if (start.y == 1 && end.y == 3
                 && board.getPiece(Position(start.x, start.y + 1)) == nullptr
                 && pend == nullptr) return SUCCESS;    //jump two on first move
        else if (pend != nullptr && pend->owner() == BLACK && (end.y == start.y + 1
                 && std::abs((float)end.x - start.x) == 1)) return MOVE_CAPTURE;    //diagonal capture
        else if (end.y != start.y + 1 || end.x != start.x) return MOVE_ERROR_ILLEGAL;    //movement rule
        else if (pend != nullptr) return
                MOVE_ERROR_BLOCKED;    //destination occupied by friendly or hostile
        return SUCCESS;
    } else if (board.playerTurn() == BLACK) {
        if (start.y == 6 && end.y == 4 && (board.getPiece({ start.x, start.y - 2 }) != nullptr
                                           || board.getPiece(Position(start.x, start.y - 1)) != nullptr))
            return MOVE_ERROR_BLOCKED;    //blocked on double jump
        else if (start.y == 6 && end.y == 4
                 && board.getPiece(Position(start.x, start.y - 1)) == nullptr && pend == nullptr)
            return SUCCESS;    //jump two on first move
        else if (pend != nullptr && pend->owner() == WHITE && (end.y == start.y - 1
                 && std::abs((float)end.x - start.x) == 1)) return MOVE_CAPTURE;    //diagonal capture
        else if (end.y != start.y - 1 || end.x != start.x) return MOVE_ERROR_ILLEGAL;    //movement rule
        else if (pend != nullptr) return
                MOVE_ERROR_BLOCKED;    //destination occupied by friendly or hostile
        return SUCCESS;
    }
    return SUCCESS;
}

//check valid move from knight
int Knight::validMove(Position start, Position end, const Board& board) const {
    Piece* pstart = board.getPiece(start);
    Piece* pend = board.getPiece(end);
    bool lawful = false;
    if (std::abs((float)end.x - start.x) == 1 && std::abs((float)end.y - start.y) == 2) lawful = true;
    else if (std::abs((float)end.x - start.x) == 2
             && std::abs((float)end.y - start.y) == 1) lawful = true;
    if (lawful && pend == nullptr) return SUCCESS;
    if (lawful && pstart->owner() != pend->owner()) return MOVE_CAPTURE;
    if (lawful && pstart->owner() == pend->owner()) return MOVE_ERROR_BLOCKED;
    return MOVE_ERROR_ILLEGAL; //not lawful
}

//check valid move from Rook
int Rook::validMove(Position start, Position end, const Board& board) const {
    Piece* pend = board.getPiece(end);
    Piece* pstart = board.getPiece(start);
    bool lawful = false;
    int diffx = end.x - start.x;
    int diffy = end.y - start.y;
    if (diffy == 0) {
        lawful = true;
        for (int i = 1; i < std::abs(diffx); i++) {
            Position temp;
            if (diffx < 0) temp = { start.x - i, start.y };
            else temp = { start.x + i, start.y };
            if (board.getPiece(temp) != nullptr) return MOVE_ERROR_BLOCKED;
        }
    } else if (diffx == 0) {
        lawful = true;
        for (int i = 1; i < std::abs(diffy); i++) {
            Position temp;
            if (diffy < 0) temp = { start.x, start.y - i };
            else temp = { start.x, start.y + i };
            if (board.getPiece(temp) != nullptr) return MOVE_ERROR_BLOCKED;
        }
    }
    if (lawful && pend == nullptr) return SUCCESS;
    if (lawful && pstart->owner() != pend->owner()) return MOVE_CAPTURE;
    if (lawful && pstart->owner() == pend->owner()) return MOVE_ERROR_BLOCKED;
    return MOVE_ERROR_ILLEGAL; //not lawful
}

//check valid move from bishop
int Bishop::validMove(Position start, Position end, const Board& board) const {
    Piece* pend = board.getPiece(end);
    Piece* pstart = board.getPiece(start);
    bool lawful = false;
    int diffx = end.x - start.x;
    int diffy = end.y - start.y;
    if (std::abs(diffx) == std::abs(diffy)) {
        lawful = true;
        for (int i = 1; i < std::abs(diffx); i++) {
            Position temp;
            if (diffx < 0 && diffy < 0) temp = { start.x - i, start.y - i };
            else if (diffx > 0 && diffy < 0) temp = { start.x + i, start.y - i };
            else if (diffx < 0 && diffy > 0) temp = { start.x - i, start.y + i };
            else temp = { start.x + i, start.y + i };
            if (board.getPiece(temp) != nullptr) {
                lawful = false;
                return MOVE_ERROR_BLOCKED;
            }
        }
    }
    if (lawful && pend == nullptr) return SUCCESS;
    if (lawful && pstart->owner() != pend->owner()) return MOVE_CAPTURE;
    if (lawful && pstart->owner() == pend->owner()) return MOVE_ERROR_BLOCKED;
    return MOVE_ERROR_ILLEGAL; //not lawful
}

//check valid move from queen
int Queen::validMove(Position start, Position end, const Board& board) const {
    Piece* pend = board.getPiece(end);
    Piece* pstart = board.getPiece(start);
    bool lawful = false;
    int diffx = end.x - start.x;
    int diffy = end.y - start.y;
    if (diffy == 0) {
        lawful = true;
        for (int i = 1; i < std::abs(diffx); i++) {
            Position temp;
            if (diffx < 0) temp = { start.x - i, start.y };
            else temp = { start.x + i, start.y };
            if (board.getPiece(temp) != nullptr) {
                lawful = false;
                return MOVE_ERROR_BLOCKED;
            }
        }
    } else if (diffx == 0) {
        lawful = true;
        for (int i = 1; i < std::abs(diffy); i++) {
            Position temp;
            if (diffy < 0) temp = { start.x, start.y - i };
            else temp = { start.x, start.y + i };
            if (board.getPiece(temp) != nullptr) {
                lawful = false;
                return MOVE_ERROR_BLOCKED;
            }
        }
    } else if (std::abs(diffx) == std::abs(diffy)) {
        lawful = true;
        for (int i = 1; i < std::abs(diffx); i++) {
            Position temp;
            if (diffx < 0 && diffy < 0) temp = { start.x - i, start.y - i };
            else if (diffx > 0 && diffy < 0) temp = { start.x + i, start.y - i };
            else if (diffx < 0 && diffy > 0) temp = { start.x - i, start.y + i };
            else temp = { start.x + i, start.y + i };
            if (board.getPiece(temp) != nullptr) {
                lawful = false;
                return MOVE_ERROR_BLOCKED;
            }
        }
    }
    if (lawful && pend == nullptr) return SUCCESS;
    if (lawful && pstart->owner() != pend->owner())return MOVE_CAPTURE;
    if (lawful && pstart->owner() == pend->owner()) return MOVE_ERROR_BLOCKED;
    return MOVE_ERROR_ILLEGAL; //not lawful
}

//check valid move from king
int King::validMove(Position start, Position end, const Board& board)const {
    Piece* pstart = board.getPiece(start);
    Piece* pend = board.getPiece(end);
    int lawful = 0;
    int diffx = end.x - start.x;
    int diffy = end.y - start.y;
    //std::cout << diffy << ',' << diffx << std::endl;
    if ((std::abs((float)diffx) <= 1) && (std::abs((float)diffy) <= 1)) {
        lawful = 1;
    }
    if (diffx == 2 || diffx == -2) {
        lawful = 2;
    }
    if (lawful == 1 && pend == nullptr) {
        return SUCCESS;
    }
    if (lawful == 1 && pstart->owner() != pend->owner()) {
        return MOVE_CAPTURE;
    }
    if (lawful == 1 && pstart->owner() == pend->owner()) {
        return MOVE_ERROR_BLOCKED;    //land on own piece
    }
    if (lawful == 2) {
        if (pstart->owner() == WHITE) {
            if (diffx == 2) {
                if (board.getPiece({ 6, 0 }) != nullptr || board.getPiece({ 5, 0 }) != nullptr) {
                    return MOVE_ERROR_CANT_CASTLE;
                }
                else return TRY_CASTLE;
            } else {
                if (board.getPiece({ 1, 0 }) != nullptr || board.getPiece({ 2, 0 }) != nullptr || board.getPiece({3, 0})
                        != nullptr) {
                    return MOVE_ERROR_CANT_CASTLE;
                }
                else return TRY_CASTLE;
            }
        } else {
            if (diffx == 2) {
                if (board.getPiece({ 6, 7 }) != nullptr || board.getPiece({ 5, 7 }) != nullptr) {
                    return MOVE_ERROR_CANT_CASTLE;
                }
                else return TRY_CASTLE;
            } else {
                if (board.getPiece({ 1, 7 }) != nullptr || board.getPiece({ 2, 7 }) != nullptr || board.getPiece({3, 7})
                        != nullptr) {
                    return MOVE_ERROR_CANT_CASTLE;
                }
                else return TRY_CASTLE;
            }
        }
    }
    return MOVE_ERROR_ILLEGAL; //not lawful
}

//create a new mystery piece and test it.
int Mystery::validMove(Position start, Position end, const Board& board)const {
    Piece* pend = board.getPiece(end);
    Piece* pstart = board.getPiece(start);
    bool lawful = false;
    int diffx = end.x - start.x;
    int diffy = end.y - start.y;
    if (diffx == 0 && diffy == 2) {
        lawful = true;
        if (board.getPiece({start.x, start.y + 1}) != nullptr) {
            return MOVE_ERROR_BLOCKED;
        }
    } else if (diffx == 0 && diffy == -2) {
        lawful = true;
        if (board.getPiece({start.x, start.y - 1}) != nullptr) {
            return MOVE_ERROR_BLOCKED;
        }
    }
    if (diffy == 0 && diffx == 2) {
        lawful = true;
        if (board.getPiece({start.x + 1, start.y}) != nullptr) {
            return MOVE_ERROR_BLOCKED;
        }
    } else if (diffy == 0 && diffx == -2) {
        lawful = true;
        if (board.getPiece({start.x - 1, start.y}) != nullptr) {
            return MOVE_ERROR_BLOCKED;
        }
    }
    if (lawful && pend == nullptr) return SUCCESS;
    if (lawful && pstart->owner() != pend->owner())return MOVE_CAPTURE;
    if (lawful && pstart->owner() == pend->owner()) return MOVE_ERROR_BLOCKED;
    return MOVE_ERROR_ILLEGAL; //not lawful
}
