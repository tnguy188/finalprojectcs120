#include <string>
#include <fstream>
#include <iostream>
#include <string>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

int main() {
    ChessGame chess, chess2;
    int check = 0;
    std::ifstream input;
    for (int i = 0; i < 2; i++) {
        if (i == 0) input.open("unittest1.txt");
        else if (i == 1) input.open("unittest2.txt");
        if (!input.is_open()) return 1;
        else {
            std::string line;
            input >> line;
            if (line != "chess") check = 1;
            int a, c;
            std::string b;
            input >> a;
            if (i == 0) chess.setTurn(a + 1);
            else if (i == 1) chess2.setTurn (a + 1);
            while (input >> a >> b >> c) {
                Position p = Position (b[0] - 97, b[1] - 49);
                Player name = a ? BLACK : WHITE;
                if (i == 0) chess.initPiece(c, name, p);
                else if (i == 1) chess2.initPiece(c, name, p);
            }
        }
        input.close();
    }


    //check if either is in the checked position (should not be)
    if (chess.isCheckedRook({2, 7}, BLACK) == true) check = 2;
    if (chess.isCheckedBishop({2, 7}, BLACK) == true) check = 2;
    if (chess.isCheckedKnight({2, 7}, BLACK) == true) check = 2;
    if (chess.isCheckedPawn({2, 7}, BLACK) == true) check = 2;
    if (chess.isCheckedKing({2, 7}, BLACK) == true) check = 2;

    if (chess.isCheckedRook({6, 0}, WHITE) == true) check = 3;
    if (chess.isCheckedBishop({6, 0}, WHITE) == true) check = 3;
    if (chess.isCheckedKnight({6, 0}, WHITE) == true) check = 3;
    if (chess.isCheckedPawn({6, 0}, WHITE) == true) check = 3;
    if (chess.isCheckedKing({6, 0}, WHITE) == true) check = 3;
    //this has shown the isChecked method works

    //since makeMove has massive amount of checks in there, it can run into
    //a lot of trouble if we isolate makeMove and test it alone.
    //We will not test makeMove but instead check if the move is valid

    chess.setTurn(BLACK);
    Piece* tempPiece = chess.getPiece({7, 1});
    if (tempPiece->validMove({7, 1}, {7, 2}, chess) != SUCCESS) check = 4;
    if (tempPiece->validMove({7, 1}, {7, 3}, chess) != SUCCESS) check = 4;
    if (tempPiece->validMove({7, 1}, {6, 2}, chess) != MOVE_ERROR_ILLEGAL)
        check = 4;
    tempPiece = chess.getPiece({3, 5});
    if (tempPiece->validMove({3, 5}, {2, 6}, chess) != MOVE_CAPTURE) check = 4;
    if (tempPiece->validMove({3, 5}, {3, 7}, chess) != MOVE_ERROR_ILLEGAL) check = 4;

    //since the black pawn is symmetric, we don't need to test them

    //test black rook movement
    chess.setTurn(WHITE);
    tempPiece = chess.getPiece({0, 4});
    if (tempPiece->validMove({0, 4}, {0, 3}, chess) != SUCCESS) check = 5;
    if (tempPiece->validMove({0, 4}, {2, 4}, chess) != SUCCESS) check = 5;
    if (tempPiece->validMove({0, 4}, {2, 5}, chess) != MOVE_ERROR_ILLEGAL) check = 5;
    if (tempPiece->validMove({0, 4}, {0, 7}, chess) != MOVE_ERROR_BLOCKED) check = 5;

    //test black bishop movement
    tempPiece = chess.getPiece({4, 7});
    if (tempPiece->validMove({4, 7}, {2, 5}, chess) !=  SUCCESS) check = 6;
    if (tempPiece->validMove({4, 7}, {6, 5}, chess) !=  MOVE_CAPTURE) check = 6;
    if (tempPiece->validMove({4, 7}, {4, 4}, chess) !=  MOVE_ERROR_ILLEGAL) check = 6;
    if (tempPiece->validMove({4, 7}, {7, 4}, chess) != MOVE_ERROR_BLOCKED) check = 6;

    //check the movement of the white king
    chess.setTurn(BLACK);
    tempPiece = chess2.getPiece({4, 0});
    if (tempPiece->validMove({4, 0}, {6, 0}, chess2) != TRY_CASTLE) check = 7;
    if (tempPiece->validMove({4, 0}, {2, 0}, chess2) != MOVE_ERROR_CANT_CASTLE) check = 7;
    if (tempPiece->validMove({4, 0}, {5, 0}, chess2) != SUCCESS) check = 7;
    if (tempPiece->validMove({4, 0}, {4, 2}, chess2) != MOVE_ERROR_ILLEGAL) check = 7;

    if (!check) std::cout << "PASS ALL TESTS" << std::endl;
    else std::cout << "Error at " << check << std::endl;
}
